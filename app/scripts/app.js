'use strict';

/**
 * @ngdoc overview
 * @name hernanhApp
 * @description
 * # hernanhApp
 *
 * Main module of the application.
 */
angular

  .module('saltalacaif', [

    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngMaterial',
    'ng-mfb',
    'mdo-angular-cryptography'
  ])

  .config(['$cryptoProvider', function($cryptoProvider) {
    $cryptoProvider.setCryptographyKey('SALTALA');
  }])

  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.hashPrefix('');

    $stateProvider
      .state('inicio', {
        url: '/',
        templateUrl: 'views/inicio.html',
        controller: 'InicioCtrl',
        controllerAs: 'inicio'
      })
      
    $urlRouterProvider.otherwise('/');

  });
